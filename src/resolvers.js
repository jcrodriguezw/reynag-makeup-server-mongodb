import { Member } from './models'
import { Link } from './models'

export const resolvers = {
    Query: {
        members: () => Member.find()
    },
    Mutation: {
        createMember: (_, { data }) => {
            const member = new Member({ name: data.name, nick: data.nick })
            member.save()
            return member
        },
        createLink: (_, { data }) => {
            const link = new Link({url: data.url, comment: data.comment,})
            link.save()
            return link
        }
    }
};