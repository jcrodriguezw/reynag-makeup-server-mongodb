import mongoose from 'mongoose'

export const Member = mongoose.model('Member', { name: String, nick: String })
export const Link = mongoose.model('Link', { url: String, comment: String})