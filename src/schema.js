import { ApolloServer, gql } from 'apollo-server'

export const typeDefs = gql`

    type Member {
        id: ID!
        name: String!
        nick: String!
    }

    type Link {
        id: ID!
        url: String!
        comment: String
      }

    type Query {
        members: [Member!]
        links: [Link!]
    }

    input MemberCreateInput {
        id: ID
        name: String!
        nick: String!
    }

    input LinkCreateInput {
        id: ID
        url: String
        comment: String
    }

    type Mutation {
        createMember(data: MemberCreateInput) : Member!
        createLink(data: LinkCreateInput!): Link!
    }
`