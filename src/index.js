import { ApolloServer, gql } from 'apollo-server'
import {typeDefs} from './schema'
import {resolvers} from './resolvers'
import mongoose from 'mongoose'

mongoose.connect('mongodb://localhost/reynag-makeup-server', {useNewUrlParser: true});

const server = new ApolloServer({ typeDefs, resolvers });

server.listen(8000).then(({ url }) => {
    console.log(`🚀  Server ready at ${url}`);
});
